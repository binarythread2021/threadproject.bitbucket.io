export const fileReader = file =>
  new Promise((resolve, reject) => {
    const image = new FileReader();
    image.onload = () => {
      resolve(image.result);
    };
    image.onerror = reject;
    image.readAsDataURL(file);
  });
