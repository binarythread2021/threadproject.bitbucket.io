import { CustomError } from '../../errors/errors';

export class UnauthorizedError extends CustomError {}
