const IconSize = {
  MINI: 'mini',
  SMALL: 'small',
  LARGE: 'large',
  TINY: 'tiny'
};

export { IconSize };
