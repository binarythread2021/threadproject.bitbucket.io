const ButtonColor = {
  TEAL: 'teal',
  BLUE: 'blue',
  GREEN: 'green'
};

export { ButtonColor };
