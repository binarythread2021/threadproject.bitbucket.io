const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  FOGOT_PASSWORD: '/fogot-password',
  RESET_PASSWORD: '/reset-password',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
