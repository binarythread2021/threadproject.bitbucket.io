import PropTypes from 'prop-types';
import { imageType } from 'src/common/prop-types/image';

const PopupLike = PropTypes.arrayOf(
  PropTypes.exact({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    image: imageType
  })
);

export { PopupLike };
