import PropTypes from 'prop-types';

const FilterProps = PropTypes.exact({
  userId: PropTypes.string,
  notOwnPosts: PropTypes.string,
  from: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  showLikePosts: PropTypes.string
});

export { FilterProps };
