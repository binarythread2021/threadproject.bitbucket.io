import { HttpMethod, ContentType } from 'src/common/enums/enums';

class CommentReaction {
  constructor({ http }) {
    this._http = http;
  }

  getCommentReactions(payload) {
    return this._http.load(`/api/comments/react`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { CommentReaction };
