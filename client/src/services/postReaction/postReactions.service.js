import { HttpMethod, ContentType } from 'src/common/enums/enums';

class PostReaction {
  constructor({ http }) {
    this._http = http;
  }

  getPostReactions(payload) {
    return this._http.load(`/api/posts/react`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { PostReaction };
