import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  _load(path, payload) {
    return this._http.load(path, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  login(payload) {
    return this._load('/api/auth/login', payload);
  }

  registration(payload) {
    return this._load('/api/auth/register', payload);
  }

  getCurrentUser() {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.GET
    });
  }

  fogotPassword(payload) {
    return this._load('/api/auth/fogot-password', payload);
  }

  resetPassword(payload) {
    return this._load('/api/auth/reset-password', payload);
  }
}

export { Auth };
