import { HttpMethod, ContentType } from 'src/common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  uploadFields(payload) {
    return this._http.load('/api/user/profile', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  uploadImage(payload) {
    return this._http.load('/api/user/image', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  getAllUsers() {
    return this._http.load('/api/user', {
      method: HttpMethod.GET
    });
  }
}

export { User };
