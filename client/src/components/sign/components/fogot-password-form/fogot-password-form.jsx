import * as React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';
import styles from './styles.module.scss';

const FogotPasswordForm = ({ onFogotPassword, loading }) => {
  const [email, setEmail] = React.useState('');
  const [isEmailValid, setIsEmailValid] = React.useState(true);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleFogotPassword = () => {
    const isValid = isEmailValid;
    if (!isValid || loading) {
      return;
    }
    onFogotPassword({ email });
  };

  return (
    <>
      <h2 className={styles.title}>Reset password: enter your email</h2>
      <Form name="resetForm" size="large" onSubmit={handleFogotPassword}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={loading}
            isDisabled={loading}
            isFluid
            isPrimary
          >
            Reset password
          </Button>
        </Segment>
      </Form>
      <Message>
        Alredy with us?{' '}
        <NavLink exact to={AppRoute.LOGIN}>
          Sign In
        </NavLink>
      </Message>
    </>
  );
};

FogotPasswordForm.propTypes = {
  onFogotPassword: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};

export default FogotPasswordForm;
