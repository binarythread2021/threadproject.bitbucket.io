import * as React from 'react';
import PropTypes from 'prop-types';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';
import styles from './styles.module.scss';

const ResetPasswordForm = ({ onResetPassword, loading }) => {
  const [password, setPassword] = React.useState('');
  const [secondPassword, setSecondPassword] = React.useState('');
  const [isPasswordValid, setPasswordValid] = React.useState(true);

  const handlePasswordBlur = () => {
    setPasswordValid(secondPassword === password);
  };

  const register = async () => {
    if (!isPasswordValid || loading) {
      return;
    }
    onResetPassword({ password });
  };

  return (
    <>
      <h2 className={styles.title}>Register for free account</h2>
      <Form name="registrationForm" size="large" onSubmit={register}>
        <Segment>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            onChange={ev => setPassword(ev.target.value)}
            error={!isPasswordValid}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Repeat Password"
            type="password"
            onChange={ev => setSecondPassword(ev.target.value)}
            error={!isPasswordValid}
            onBlur={handlePasswordBlur}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={loading}
            isDisabled={loading}
            isFluid
            isPrimary
          >
            Reset Password
          </Button>
        </Segment>
      </Form>
      <Message>
        Alredy with us?{' '}
        <NavLink exact to={AppRoute.LOGIN}>
          Sign In
        </NavLink>
      </Message>
    </>
  );
};

ResetPasswordForm.propTypes = {
  onResetPassword: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};

export default ResetPasswordForm;
