import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import FogotPasswordForm from './fogot-password-form/fogot-password-form';
import ResetPasswordForm from './reset-password-form/reset-password-form';

export { LoginForm, RegistrationForm, FogotPasswordForm, ResetPasswordForm };
