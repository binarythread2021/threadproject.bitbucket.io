import * as React from 'react';
import { useLocation, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AppRoute } from 'src/common/enums/enums';
import { profileActionCreator } from 'src/store/actions';
import { Grid, Image } from 'src/components/common/common';
import {
  LoginForm,
  RegistrationForm,
  FogotPasswordForm,
  ResetPasswordForm
} from './components/components';
import styles from './styles.module.scss';

const Login = () => {
  const dispatch = useDispatch();
  const { pathname, search } = useLocation();
  const [query, setQuery] = React.useState(new Map());
  const { user, loading } = useSelector(state => state.profile);

  const hasUser = Boolean(user);

  React.useEffect(() => {
    setQuery(new URLSearchParams(search));
  }, [search]);

  const handleLogin = React.useCallback(
    loginPayload => dispatch(profileActionCreator.login(loginPayload)),
    [dispatch]
  );

  const handleResetPassword = React.useCallback(
    resetPayload => {
      dispatch(
        profileActionCreator.resetPassword({
          ...resetPayload,
          token: query.get('token') ?? '',
          userId: query.get('id') ?? ''
        })
      );
    },
    [dispatch, query]
  );
  const handleFogotPassword = React.useCallback(
    fogotPayload => {
      dispatch(profileActionCreator.fogotPassword(fogotPayload));
    },
    [dispatch]
  );

  const handleRegister = React.useCallback(
    registerPayload => dispatch(profileActionCreator.register(registerPayload)),
    [dispatch]
  );

  const getScreen = path => {
    switch (path) {
      case AppRoute.LOGIN: {
        return <LoginForm onLogin={handleLogin} loading={loading} />;
      }
      case AppRoute.REGISTRATION: {
        return (
          <RegistrationForm onRegister={handleRegister} loading={loading} />
        );
      }
      case AppRoute.FOGOT_PASSWORD: {
        return (
          <FogotPasswordForm
            onFogotPassword={handleFogotPassword}
            loading={loading}
          />
        );
      }
      case AppRoute.RESET_PASSWORD: {
        return (
          <ResetPasswordForm
            onResetPassword={handleResetPassword}
            loading={loading}
          />
        );
      }
      default: {
        return null;
      }
    }
  };

  if (hasUser) {
    return <Redirect to={{ pathname: AppRoute.ROOT }} />;
  }

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <h2 className={styles.logoWrapper}>
          <Image
            width="75"
            height="75"
            circular
            src="http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png"
          />
          Thread
        </h2>
        {getScreen(pathname)}
      </Grid.Column>
    </Grid>
  );
};

export default Login;
