import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { List } from 'semantic-ui-react';
import { Input, Spinner } from 'src/components/common/common';
import { profileActionCreator } from 'src/store/actions';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Input as CustomInput, InputImage } from './components';

import styles from './styles.module.scss';

const Profile = () => {
  const { user, loading } = useSelector(state => ({
    user: state.profile.user,
    loading: state.profile.loading
  }));
  const dispatch = useDispatch();
  const handleChangeName = React.useCallback(
    name => {
      dispatch(profileActionCreator.uploadFields('username', name));
    },
    [dispatch]
  );
  const handleChnageStatus = React.useCallback(
    status => {
      dispatch(profileActionCreator.uploadFields('status', status));
    },
    [dispatch]
  );
  const handleSaveImage = React.useCallback(
    image => {
      dispatch(profileActionCreator.uploadImage(image, user));
    },
    [dispatch, user]
  );
  return (
    <div className={styles.wrapper}>
      {loading ? (
        <Spinner />
      ) : (
        <List divided relaxed>
          <List.Item>
            <InputImage
              src={user.image?.link ?? DEFAULT_USER_AVATAR}
              onSave={handleSaveImage}
            />
          </List.Item>
          <List.Item>
            <CustomInput
              value={user.username}
              onChangeValue={handleChangeName}
              icon="user"
              placeholder="Name"
            />
          </List.Item>
          <List.Item>
            <div className={styles.wrapperEmail}>
              <Input
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                disabled
                value={user.email}
              />
            </div>
          </List.Item>
          <List.Item>
            <CustomInput
              value={user?.status ?? '-'}
              onChangeValue={handleChnageStatus}
              icon="exclamation"
              placeholder="Status"
            />
          </List.Item>
        </List>
      )}
    </div>
  );
};

export default Profile;
