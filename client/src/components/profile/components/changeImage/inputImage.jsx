import React from 'react';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Image, Button } from 'src/components/common/common';
import { AvatarProfileSize } from 'src/common/enums/enums';
import { fileReader } from 'src/helpers/helpers';
import { ChangeSizeImage } from './changeSizeImage';

import styles from './styles.module.scss';

const validationImg = (file, resolution) =>
  new Promise((resolve, reject) => {
    const image = document.createElement('img');
    const loadReader = async () => {
      const src = await fileReader(file);
      image.onload = () => {
        const width = image.naturalWidth;
        const height = image.naturalHeight;
        resolve({
          valid: width <= resolution.width && height <= resolution.height,
          src
        });
      };
      image.onerror = () => reject(new Error('Wrong image!'));
      image.src = src;
    };
    loadReader();
  });

const InputImage = ({ src, onSave }) => {
  const [loading, setLoading] = React.useState(false);
  const [mounted, setMounted] = React.useState(false);
  const [upSrc, setUpSrc] = React.useState(null);
  const handleChange = async ev => {
    const { files } = ev.target;
    if (!files || (files && !files.length)) {
      return;
    }
    setLoading(true);
    try {
      const file = files[0];
      const fileValid = await validationImg(file, AvatarProfileSize);
      if (!mounted) {
        return;
      }
      if (!fileValid.valid) {
        setUpSrc(fileValid.src);
      } else {
        onSave(file);
      }
    } catch (error) {
      if (!mounted) {
        return;
      }
      NotificationManager.error(error?.message ?? 'somthing wrong');
    }
    setLoading(false);
  };

  React.useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);

  const handleCLoseChangeSize = () => {
    setUpSrc(null);
  };

  const handleOnSave = imageBlob => {
    if (!imageBlob) {
      return;
    }
    onSave(imageBlob);
  };

  return (
    <div className={styles.imageWrapper}>
      <Image
        centered
        src={src ?? DEFAULT_USER_AVATAR}
        size="medium"
        circular
        className={styles.image}
      />
      <Button color="teal" isLoading={loading}>
        <label className={styles.label}>
          Change avatar
          <input
            name="image"
            type="file"
            accept="image/*"
            onChange={handleChange}
            hidden
          />
        </label>
      </Button>
      {upSrc && (
        <ChangeSizeImage
          src={upSrc}
          onClose={handleCLoseChangeSize}
          onSave={handleOnSave}
        />
      )}
    </div>
  );
};

InputImage.propTypes = {
  onSave: PropTypes.func.isRequired,
  src: PropTypes.string
};

InputImage.defaultProps = {
  src: undefined
};

export { InputImage };
