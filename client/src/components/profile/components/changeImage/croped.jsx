import React from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import { Button } from 'src/components/common/common';
import { ButtonColor, AvatarProfileSize } from 'src/common/enums/enums';

import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';

const Croped = ({ src, onSave }) => {
  const imgRef = React.useRef(null);
  const [mounted, setMounted] = React.useState(false);
  const previewCanvasRef = React.useRef(null);
  const [crop, setCrop] = React.useState({
    unit: 'px',
    width: AvatarProfileSize.width,
    aspect: 1
  });
  const [completedCrop, setCompletedCrop] = React.useState(null);
  const onLoad = React.useCallback(img => {
    imgRef.current = img;
  }, []);

  const handleOnSave = async () => {
    if (!completedCrop || !previewCanvasRef.current) {
      onSave();
      return;
    }
    const file = await new Promise(resolve => {
      previewCanvasRef.current.toBlob(blob => {
        resolve(new File([blob], 'avatar.jpg'));
      });
    });
    if (!mounted) {
      return;
    }
    onSave(file);
  };

  React.useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);

  React.useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const cropCom = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = cropCom.width * pixelRatio;
    canvas.height = cropCom.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      cropCom.x * scaleX,
      cropCom.y * scaleY,
      cropCom.width * scaleX,
      cropCom.height * scaleY,
      0,
      0,
      cropCom.width,
      cropCom.height
    );
  }, [completedCrop]);

  return (
    <>
      <ReactCrop
        keepSelection
        locked
        src={src}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => setCrop(c)}
        onComplete={c => setCompletedCrop(c)}
        className={styles.crop}
      />
      <div className={styles.cropFooter}>
        <canvas
          ref={previewCanvasRef}
          style={{
            width: Math.round(completedCrop?.width ?? 0),
            height: Math.round(completedCrop?.height ?? 0)
          }}
        />
        <Button
          className={styles.button}
          color={ButtonColor.BLUE}
          isDisabled={!completedCrop?.width || !completedCrop?.height}
          onClick={handleOnSave}
        >
          Save
        </Button>
      </div>
    </>
  );
};

Croped.propTypes = {
  src: PropTypes.string.isRequired,
  onSave: PropTypes.func.isRequired
};

export { Croped };
