import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'src/components/common/common';
import { Croped } from './croped';

import styles from './styles.module.scss';

const ChangeSizeImage = ({ src, onClose, onSave }) => {
  const [open, setOpen] = React.useState(true);
  const handleCloseModel = () => {
    setOpen(false);
    onClose();
  };

  const handleOnSave = imageBlob => {
    setOpen(false);
    onSave(imageBlob);
    onClose();
  };

  return (
    <Modal open={open} closeIcon onClose={handleCloseModel}>
      <div className={styles.cropedWrapper}>
        <Croped src={src} onSave={handleOnSave} />
      </div>
    </Modal>
  );
};

ChangeSizeImage.propTypes = {
  src: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};

export { ChangeSizeImage };
