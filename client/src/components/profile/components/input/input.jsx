import React from 'react';
import PropTypes from 'prop-types';
import { Input as DefaultInput, Button } from 'src/components/common/common';
import { ButtonColor } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const Input = ({ value, onChangeValue, icon, placeholder }) => {
  const [upValue, setUpValue] = React.useState(value);
  const [onChange, setOnChange] = React.useState(false);
  const [changeDisabled, setChangeDisabled] = React.useState(true);
  const handleClickSave = () => {
    onChangeValue(upValue);
    setOnChange(false);
  };
  const handleChange = ev => {
    const newValue = ev.target.value;
    setUpValue(newValue);
    setChangeDisabled(!newValue.length || newValue === value);
  };
  const handleClickChange = () => {
    setOnChange(state => {
      if (state) {
        setUpValue(value);
        setChangeDisabled(true);
      }
      return !state;
    });
  };
  return (
    <div className={styles.wrapperInput}>
      <DefaultInput
        icon={icon}
        iconPosition="left"
        placeholder={placeholder}
        type="text"
        disabled={!onChange}
        value={upValue}
        onChange={handleChange}
        className={styles.input}
      />
      {onChange ? (
        <div className={styles.buttonGroup}>
          <Button
            color={ButtonColor.BLUE}
            isDisabled={changeDisabled}
            onClick={handleClickSave}
          >
            Save
          </Button>
          <Button color={ButtonColor.BLUE} onClick={handleClickChange}>
            Cancel
          </Button>
        </div>
      ) : (
        <Button color={ButtonColor.TEAL} onClick={handleClickChange}>
          Change
        </Button>
      )}
    </div>
  );
};

Input.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeValue: PropTypes.func.isRequired,
  icon: PropTypes.string.isRequired,
  placeholder: PropTypes.string
};

Input.defaultProps = {
  placeholder: undefined
};

export { Input };
