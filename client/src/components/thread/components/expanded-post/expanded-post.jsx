import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import {
  threadCommentActionCreator,
  threadActionCreator
} from 'src/store/actions';
import {
  Spinner,
  Post,
  Modal,
  Comment as CommentUI
} from 'src/components/common/common';
import { AddComment, UpdateComment } from '../add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({ sharePost }) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sortedComments, setSortedComment] = React.useState([]);
  const [editComment, setEditComment] = React.useState(false);
  const [open, setOpen] = React.useState(true);

  const handlePostLike = React.useCallback(
    (id, isLike = true) => dispatch(threadActionCreator.likePost(id, isLike)),
    [dispatch]
  );

  const handleCommentLike = React.useCallback(
    (id, isLike = true) =>
      dispatch(threadCommentActionCreator.likeComment(id, post.id, isLike)),
    [dispatch, post.id]
  );

  const handleCommentDelete = React.useCallback(
    id => dispatch(threadCommentActionCreator.deleteComment(id, post.id)),
    [dispatch, post.id]
  );

  const handleClikButtonUpdate = id => {
    setEditComment(sortedComments.filter(comment => comment.id === id)[0]);
    const comments = post.comments.filter(comment => comment.id !== id);
    setSortedComment(getSortedComments(comments));
  };

  const handleCommentUpdate = React.useCallback(
    (commentId, payload, postId) => {
      if (commentId) {
        dispatch(
          threadCommentActionCreator.updatedComment(commentId, payload, postId)
        );
      } else {
        setSortedComment(getSortedComments(post.comments));
      }
      setEditComment(false);
    },
    [dispatch, post.comments]
  );

  const handleCommentAdd = React.useCallback(
    commentPayload =>
      dispatch(threadCommentActionCreator.addComment(commentPayload)),
    [dispatch]
  );

  const handleExpandedPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  React.useEffect(() => {
    if (open) {
      return;
    }
    handleExpandedPostToggle();
  }, [open, handleExpandedPostToggle]);

  React.useEffect(() => {
    setSortedComment(getSortedComments(post.comments));
  }, [post.comments]);

  return (
    <Modal centered={false} open={open} onClose={() => setOpen(false)}>
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                onLike={handleCommentLike}
                onDelete={handleCommentDelete}
                onUpdate={handleClikButtonUpdate}
                userId={userId}
              />
            ))}
            {editComment ? (
              <UpdateComment
                postId={post.id}
                comment={editComment}
                commentSave={handleCommentUpdate}
              />
            ) : (
              <AddComment postId={post.id} commentSave={handleCommentAdd} />
            )}
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
