import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import {
  Comment as CommentUI,
  PopupLikeComment
} from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import {
  SocialButton,
  ButtonsChange
} from 'src/components/common/post/components';

import styles from './styles.module.scss';

const Comment = ({ onLike, comment, onDelete, onUpdate, userId }) => {
  const { body, createdAt, user, likeCount, dislikeCount, id } = comment;
  const handleLike = () => onLike(id);
  const handleDislike = () => onLike(id, false);
  const handleDelete = () => onDelete(id);
  const handleUpdate = () => onUpdate(id);
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <div className={styles.status}>{user?.status ?? '-'}</div>
        <CommentUI.Text>{body}</CommentUI.Text>
        <div className={styles.wrapperButton}>
          <div className={styles.wrapperSocial}>
            <PopupLikeComment id={id}>
              <SocialButton
                count={likeCount}
                onClick={handleLike}
                iconName="THUMBS_UP"
              />
            </PopupLikeComment>
            <SocialButton
              count={dislikeCount}
              onClick={handleDislike}
              iconName="THUMBS_DOWN"
            />
          </div>
          {comment.user.id === userId ? (
            <ButtonsChange onDelete={handleDelete} onUpdate={handleUpdate} />
          ) : null}
        </div>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onLike: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Comment;
