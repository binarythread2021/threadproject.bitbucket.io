import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';

const labelButtons = {
  add: 'Post comment',
  update: 'Update comment'
};

const SaveComment = ({ body, onSave, type }) => {
  const [text, setText] = React.useState(body);
  const [buttonDisabled, setbuttonDisabled] = React.useState(true);
  const handleSaveComment = () => {
    if (!text || buttonDisabled) {
      return;
    }
    onSave(text);
    setText('');
    setbuttonDisabled(true);
  };

  const handleChange = ev => {
    setText(ev.target.value);
    setbuttonDisabled(false);
  };

  const handleCancel = () => {
    onSave();
  };

  return (
    <Form reply onSubmit={handleSaveComment}>
      <Form.TextArea
        value={text}
        placeholder="Type a comment..."
        onChange={handleChange}
      />
      <Button type={ButtonType.SUBMIT} isPrimary isDisabled={buttonDisabled}>
        {labelButtons[type]}
      </Button>
      {type === 'update' ? (
        <Button type={ButtonType.BUTTON} isPrimary onClick={handleCancel}>
          Cancel
        </Button>
      ) : null}
    </Form>
  );
};

SaveComment.propTypes = {
  onSave: PropTypes.func.isRequired,
  type: PropTypes.oneOf(['update', 'add']).isRequired,
  body: PropTypes.string
};

SaveComment.defaultProps = {
  body: ''
};

export default SaveComment;
