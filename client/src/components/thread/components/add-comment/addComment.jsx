import * as React from 'react';
import PropTypes from 'prop-types';
import SaveComment from './saveComment';

const AddComment = ({ postId, commentSave }) => {
  const handleSaveComment = body => {
    if (!body) {
      return;
    }
    commentSave({ postId, body });
  };

  return <SaveComment onSave={handleSaveComment} type="add" />;
};

AddComment.propTypes = {
  commentSave: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export { AddComment };
