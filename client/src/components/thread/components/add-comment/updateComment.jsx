import * as React from 'react';
import PropTypes from 'prop-types';
import { commentType } from 'src/common/prop-types/prop-types';
import SaveComment from './saveComment';

const UpdateComment = ({ postId, commentSave, comment }) => {
  const handleSaveComment = body => {
    if (!body) {
      commentSave();
    } else {
      commentSave(comment.id, { body, postId });
    }
  };
  return (
    <SaveComment body={comment.body} onSave={handleSaveComment} type="update" />
  );
};

UpdateComment.propTypes = {
  commentSave: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  comment: commentType.isRequired
};

export { UpdateComment };
