import React from 'react';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { IconName } from 'src/common/enums/enums';
import { Button } from 'src/components/common/common';
import { fileReader } from 'src/helpers/helpers';

import styles from './styles.module.scss';

const ButtonAttach = ({ onImage, onImageUpload, src, imageUpload }) => {
  const [mounted, setMounted] = React.useState(false);
  const handleUploadImg = async e => {
    const { files } = e.target;
    if (!files || (files && !files.length)) {
      return;
    }
    const image = e.target.files[0];
    onImageUpload(true);
    try {
      const srcNew = await fileReader(image);
      if (!mounted) {
        return;
      }
      onImage(image, srcNew);
    } catch (error) {
      if (!mounted) {
        return;
      }
      NotificationManager.error(error?.message ?? 'somthing wrong');
    }
    onImageUpload(false);
  };
  React.useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);
  const handleDeleteImg = () => {
    onImage(null, undefined);
  };
  return src ? (
    <Button onClick={handleDeleteImg} color="teal">
      Delete image
    </Button>
  ) : (
    <Button
      color="teal"
      isLoading={imageUpload}
      isDisabled={imageUpload}
      iconName={IconName.IMAGE}
    >
      <label className={styles.btnImgLabel}>
        Attach image
        <input
          name="image"
          type="file"
          accept="image/*"
          onChange={handleUploadImg}
          hidden
        />
      </label>
    </Button>
  );
};

ButtonAttach.propTypes = {
  onImage: PropTypes.func.isRequired,
  onImageUpload: PropTypes.func.isRequired,
  imageUpload: PropTypes.bool.isRequired,
  src: PropTypes.string
};

ButtonAttach.defaultProps = {
  src: undefined
};

export default ButtonAttach;
