import React from 'react';
import PropTypes from 'prop-types';
import { Image as ImageCustome } from 'src/components/common/common';

import styles from './styles.module.scss';

const Image = ({ src }) =>
  src ? (
    <div className={styles.imageWrapper}>
      <ImageCustome className={styles.image} src={src} alt="post" />
    </div>
  ) : null;

Image.propTypes = {
  src: PropTypes.string
};

Image.defaultProps = {
  src: undefined
};

export default Image;
