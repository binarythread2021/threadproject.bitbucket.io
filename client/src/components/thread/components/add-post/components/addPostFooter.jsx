import React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';
import { Button } from 'src/components/common/common';
import Image from './image';
import ButtonAttach from './buttonAttach';

import styles from './styles.module.scss';

const AddPostFooter = ({ src, onImage, isDisabled }) => {
  const [imageUpload, setImageUpload] = React.useState(false);
  return (
    <>
      <Image src={src} />
      <div className={styles.btnWrapper}>
        <ButtonAttach
          onImageUpload={setImageUpload}
          src={src}
          imageUpload={imageUpload}
          onImage={onImage}
        />
        <Button
          color={ButtonColor.BLUE}
          type={ButtonType.SUBMIT}
          isDisabled={imageUpload || isDisabled}
        >
          Post
        </Button>
      </div>
    </>
  );
};

AddPostFooter.propTypes = {
  src: PropTypes.string,
  onImage: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired
};

AddPostFooter.defaultProps = {
  src: undefined
};

export default AddPostFooter;
