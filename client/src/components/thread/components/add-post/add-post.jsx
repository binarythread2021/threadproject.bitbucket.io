import * as React from 'react';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import { Spinner } from 'src/components/common/common';
import ChangePost from './changePost';

const AddPost = ({ onPostAdd }) => {
  const [body, setBody] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [src, setSrc] = React.useState(undefined);
  const [image, setImage] = React.useState(null);

  const handleSubmit = async () => {
    if (!body) {
      return;
    }
    setLoading(true);
    try {
      await onPostAdd({ image, body });
      setBody('');
      setImage(null);
      setSrc(undefined);
    } catch (e) {
      NotificationManager.error(e?.messsage ?? 'Somthing wrong! Try later');
    }
    setLoading(false);
  };

  const handleImageChange = (newImage, newSrc) => {
    setImage(newImage);
    setSrc(newSrc);
  };

  return loading ? (
    <Spinner />
  ) : (
    <ChangePost
      onSubmit={handleSubmit}
      onBodyChange={value => setBody(value)}
      onImage={handleImageChange}
      body={body}
      src={src}
    />
  );
};

AddPost.propTypes = {
  onPostAdd: PropTypes.func.isRequired
};

export default AddPost;
