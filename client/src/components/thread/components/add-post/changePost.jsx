import * as React from 'react';
import PropTypes from 'prop-types';
import { Form, Segment } from 'src/components/common/common';
import { AddPostFooter } from './components';

const ChangePost = ({ onSubmit, body, onBodyChange, src, onImage }) => {
  const [isDisabled, setDisabled] = React.useState(true);
  const handleImageChange = (newImage, newSrc) => {
    setDisabled(!body.length);
    onImage(newImage, newSrc);
  };
  const handleBodyChange = ev => {
    const { value } = ev.target;
    setDisabled(!value.length);
    onBodyChange(value);
  };
  return (
    <Segment>
      <Form onSubmit={onSubmit}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={handleBodyChange}
        />
        <AddPostFooter
          src={src}
          onImage={handleImageChange}
          isDisabled={isDisabled}
        />
      </Form>
    </Segment>
  );
};

ChangePost.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onBodyChange: PropTypes.func.isRequired,
  onImage: PropTypes.func.isRequired,
  body: PropTypes.string,
  src: PropTypes.string
};

ChangePost.defaultProps = {
  body: '',
  src: undefined
};

export default ChangePost;
