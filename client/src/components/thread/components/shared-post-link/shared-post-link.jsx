import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { NotificationManager } from 'react-notifications';
import { IconName, IconColor } from 'src/common/enums/enums';
import { Icon, Modal, Input, Spinner } from 'src/components/common/common';
import { profileActionCreator } from 'src/store/actions';
import { post } from 'src/services/services';
import ByEmail from './by-email';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [isCopied, setIsCopied] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(true);
  const dispatch = useDispatch();
  let input = React.useRef();

  const copyToClipboard = ({ target }) => {
    input.select();
    document.execCommand('copy');
    target.focus();
    setIsCopied(true);
  };

  React.useEffect(() => {
    if (!open) {
      close();
    }
  }, [open, close]);

  React.useEffect(() => {
    dispatch(profileActionCreator.getAllUsers());
  }, [dispatch]);

  const handleSelect = React.useCallback(
    async userId => {
      setLoading(true);
      try {
        const info = await post.sendSharePost({ postId, userId });
        NotificationManager.info(info?.message || 'sent');
      } catch (e) {
        NotificationManager.info(e?.message || 'not sent');
      }
      setLoading(false);
    },
    [postId]
  );

  return (
    <Modal open onClose={() => setOpen(false)}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => {
            input = ref;
          }}
        />
        {loading ? <Spinner isOverflow /> : <ByEmail onSelect={handleSelect} />}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
