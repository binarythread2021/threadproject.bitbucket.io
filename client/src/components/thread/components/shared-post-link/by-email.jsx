import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { ButtonColor } from 'src/common/enums/enums';
import { Button, Dropdown, Spinner } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

import styles from './styles.module.scss';

const ByEmail = ({ onSelect }) => {
  const { users, loading } = useSelector(state => ({
    users: state.profile.users,
    loading: state.profile.loading
  }));
  const [options, setOptions] = React.useState([]);
  const [value, setValue] = React.useState('');
  const [disabled, setDisabled] = React.useState(true);
  React.useEffect(() => {
    setOptions(
      users.map(user => ({
        value: user.id,
        text: user.username,
        image: { avatar: true, src: user?.image?.link || DEFAULT_USER_AVATAR }
      }))
    );
  }, [users]);
  const handleClick = () => {
    if (!value.length) {
      return;
    }
    onSelect(value);
  };
  const handleChange = (_, data) => {
    setValue(data?.value || '');
    setDisabled(!(data?.value || '').length);
  };
  return (
    <div className={styles.wrapperByEmail}>
      {loading ? (
        <Dropdown
          placeholder="Select user"
          selection
          fluid
          options={options}
          onChange={handleChange}
        />
      ) : (
        <Spinner isOverflow />
      )}
      <Button
        color={ButtonColor.TEAL}
        onClick={handleClick}
        isDisabled={disabled}
      >
        Sent link post to email
      </Button>
    </div>
  );
};

ByEmail.propTypes = {
  onSelect: PropTypes.func.isRequired
};

export default ByEmail;
