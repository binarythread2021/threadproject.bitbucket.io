import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'src/components/common/common';
import { FilterProps } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const Filter = ({ postsFilter, onPostsLoad, userId }) => {
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = React.useState(false);
  const [showLikePosts, setShowLikePosts] = React.useState(false);
  const showPosts = () => {
    postsFilter.from = 0;
    onPostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };
  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    showPosts();
  };
  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    postsFilter.notOwnPosts = !showNotOwnPosts ? 'is' : undefined;
    postsFilter.userId = !showNotOwnPosts ? userId : undefined;
    showPosts();
  };
  const toggleShowLikePost = () => {
    setShowLikePosts(!showLikePosts);
    postsFilter.showLikePosts = !showLikePosts ? userId : undefined;
    showPosts();
  };
  return (
    <div className={styles.toolbar}>
      <Checkbox
        toggle
        disabled={showNotOwnPosts}
        label="Show only my posts"
        checked={showOwnPosts}
        onChange={toggleShowOwnPosts}
      />
      <Checkbox
        toggle
        disabled={showOwnPosts}
        label="Show not my posts"
        checked={showNotOwnPosts}
        onChange={toggleShowNotOwnPosts}
      />
      <Checkbox
        toggle
        disabled={showNotOwnPosts}
        label="Show only my like posts"
        checked={showLikePosts}
        onChange={toggleShowLikePost}
      />
    </div>
  );
};

Filter.propTypes = {
  onPostsLoad: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  postsFilter: FilterProps
};

Filter.defaultProps = {
  postsFilter: {
    userId: undefined,
    notOwnPosts: undefined,
    showLikePosts: undefined,
    from: 0,
    count: 10
  }
};

export default Filter;
