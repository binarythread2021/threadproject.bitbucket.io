import React from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'semantic-ui-react';
import { Modal } from 'src/components/common/common';
import { postType } from 'src/common/prop-types/prop-types';
import ChangePost from '../add-post/changePost';

import style from './styles.module.scss';

const UpdatePost = ({ close, post, onPostUpdate }) => {
  const [open, setOpen] = React.useState(true);
  const [body, setBody] = React.useState(post.body);
  const [src, setSrc] = React.useState(post?.image?.link);
  const [image, setImage] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [mounted, setMounted] = React.useState(false);
  React.useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);
  React.useEffect(() => {
    if (!open) {
      close();
    }
  }, [open, close]);

  const handleSubmit = async () => {
    if (!body) {
      return;
    }
    setLoading(true);
    await onPostUpdate({ image, body, post });
    if (!mounted) {
      return;
    }
    setLoading(false);
    close();
  };

  const handleImageChange = (newImage, newSrc) => {
    setImage(newImage);
    setSrc(newSrc);
  };
  return (
    <Modal open={open} onClose={() => setOpen(false)}>
      <Modal.Header>Update youre post!</Modal.Header>
      <Modal.Content>
        <ChangePost
          onSubmit={handleSubmit}
          onBodyChange={value => setBody(value)}
          onImage={handleImageChange}
          body={body}
          src={src}
        />
        {loading ? (
          <div className={style.loaderWrapper}>
            <Loader size="massive" className={style.loader}>
              Loading
            </Loader>
          </div>
        ) : null}
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: postType.isRequired,
  close: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired
};

export default UpdatePost;
