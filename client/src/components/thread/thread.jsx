import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { Post, Spinner } from 'src/components/common/common';
import {
  ExpandedPost,
  SharedPostLink,
  AddPost,
  UpdatePost,
  Filter
} from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  showLikePosts: undefined,
  notOwnPosts: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [updatePost, setUpdatePost] = React.useState(undefined);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(
    (id, isLike = true) => dispatch(threadActionCreator.likePost(id, isLike)),
    [dispatch]
  );

  const handleExpandedPostToggle = React.useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = React.useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostUpdate = React.useCallback(
    postPayload => dispatch(threadActionCreator.updatePost(postPayload)),
    [dispatch]
  );

  const handlePostDelete = React.useCallback(
    postPayload => dispatch(threadActionCreator.deletePost(postPayload)),
    [dispatch]
  );

  const handlePostForUpdate = id =>
    setUpdatePost(posts.filter(post => post.id === id)[0]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const handleCloseUpdate = React.useCallback(() => {
    setUpdatePost(false);
  }, []);

  const sharePost = id => setSharedPostId(id);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} />
      </div>
      <Filter
        onPostsLoad={handlePostsLoad}
        postsFilter={postsFilter}
        userId={userId}
      />
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            onPostDelete={handlePostDelete}
            updatePost={handlePostForUpdate}
            userId={userId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {updatePost && (
        <UpdatePost
          close={handleCloseUpdate}
          post={updatePost}
          onPostUpdate={handlePostUpdate}
        />
      )}
    </div>
  );
};

export default Thread;
