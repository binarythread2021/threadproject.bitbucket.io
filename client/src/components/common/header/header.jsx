import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import {
  IconName,
  IconSize,
  ButtonType,
  AppRoute
} from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import {
  Button,
  Icon,
  Image,
  Grid,
  NavLink
} from 'src/components/common/common';
import { profileActionCreator } from 'src/store/actions';
import { User } from './components';

import styles from './styles.module.scss';

const Header = ({ onUserLogout }) => {
  const { user, loading } = useSelector(state => state.profile);
  const dispatch = useDispatch();
  const handleChangeStatus = React.useCallback(
    status => {
      dispatch(profileActionCreator.uploadFields('status', status));
    },
    [dispatch]
  );
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <div className={styles.userWrapper}>
              <NavLink exact to={AppRoute.ROOT}>
                <Image
                  circular
                  width="45"
                  height="45"
                  src={user.image?.link ?? DEFAULT_USER_AVATAR}
                />
              </NavLink>
              <User
                username={user.username}
                loading={loading}
                status={user?.status ?? '-'}
                onChange={handleChangeStatus}
              />
            </div>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink
            exact
            activeClassName="active"
            to={AppRoute.PROFILE}
            className={styles.menuBtn}
          >
            <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
          </NavLink>
          <Button
            className={`${styles.menuBtn} ${styles.logoutBtn}`}
            onClick={onUserLogout}
            type={ButtonType.BUTTON}
            iconName={IconName.LOG_OUT}
            iconSize={IconSize.LARGE}
            isBasic
          />
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired
};

export default Header;
