import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'src/components/common/common';
import { ButtonColor, IconSize } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const EditStatus = ({ status, onCancel, onChange }) => {
  const statusInput = React.useRef(null);
  const handleClickChange = () => {
    onChange(statusInput.current.value);
  };

  React.useEffect(() => {
    if (!statusInput?.current) {
      return;
    }
    statusInput.current.focus();
  }, []);
  return (
    <div className={styles.editStatus}>
      <input
        placeholder="status"
        type="text"
        defaultValue={status}
        className={styles.input}
        ref={statusInput}
      />
      <Button
        color={ButtonColor.BLUE}
        size={IconSize.TINY}
        onClick={handleClickChange}
        className={styles.button}
      >
        udate
      </Button>
      <Button
        color={ButtonColor.BLUE}
        size={IconSize.TINY}
        onClick={() => onCancel()}
        className={styles.button}
      >
        Cancel
      </Button>
    </div>
  );
};

EditStatus.propTypes = {
  status: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

export { EditStatus };
