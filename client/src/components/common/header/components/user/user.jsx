import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'src/components/common/common';
import { IconName, IconSize } from 'src/common/enums/enums';
import { EditStatus } from './editStatus';

import styles from './styles.module.scss';

const User = ({ username, status, onChange, loading }) => {
  const [isEdit, setEdit] = React.useState(false);
  const handleChange = upStatus => {
    if (status === upStatus) {
      return;
    }
    setEdit(false);
    onChange(upStatus);
  };
  const handleOnEdit = () => {
    if (loading) {
      return;
    }
    setEdit(true);
  };
  return (
    <div>
      <div>{username}</div>
      {!isEdit ? (
        <div className={styles.userStatus}>
          <span>{status}</span>
          <Icon
            name={IconName.EDIT}
            size={IconSize.SMALL}
            onClick={handleOnEdit}
          />
        </div>
      ) : (
        <EditStatus
          status={status}
          onCancel={() => setEdit(false)}
          onChange={handleChange}
        />
      )}
    </div>
  );
};

User.propTypes = {
  status: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};

export { User };
