import React from 'react';
import { List, Image } from 'src/components/common/common';
import { PopupLike } from 'src/common/prop-types/popupLike';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

const ListUsers = React.forwardRef(({ users }, ref) => (
  <List verticalAlign="middle" ref={ref}>
    {users.map(user => (
      <List.Item key={user.id}>
        <Image avatar src={user?.image?.link ?? DEFAULT_USER_AVATAR} />
        <List.Content>
          <List.Header as="a">{user.username}</List.Header>
        </List.Content>
      </List.Item>
    ))}
  </List>
));

ListUsers.propTypes = {
  users: PopupLike.isRequired
};

export default ListUsers;
