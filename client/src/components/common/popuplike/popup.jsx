import React from 'react';
import PropTypes from 'prop-types';
import { Label, Popup as PopupDefault } from 'semantic-ui-react';
import { Spinner } from 'src/components/common/common';
import ListUser from './components/listUser';

const Popup = ({ getReactions, children }) => {
  const [isLoading, setLoading] = React.useState(true);
  const [mounted, setMounted] = React.useState(false);
  const [users, setUsers] = React.useState([]);
  const handleOpen = () => {
    const load = async () => {
      const reactions = await getReactions();
      if (!mounted) {
        return;
      }
      setLoading(false);
      setUsers(reactions.map(reaction => reaction.user));
    };
    load();
  };
  React.useEffect(() => {
    setMounted(true);
    return () => {
      setMounted(false);
    };
  }, []);
  const content = users.length ? (
    <ListUser users={users} />
  ) : (
    <Label>no likes</Label>
  );
  return (
    <PopupDefault trigger={<div>{children}</div>} onOpen={handleOpen}>
      <PopupDefault.Content>
        {isLoading ? <Spinner /> : content}
      </PopupDefault.Content>
    </PopupDefault>
  );
};

Popup.propTypes = {
  getReactions: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired
};

export { Popup };
