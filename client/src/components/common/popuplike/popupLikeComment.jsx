import React from 'react';
import PropTypes from 'prop-types';
import { commentReaction as commentReactionService } from 'src/services/services';
import { Popup } from './popup';

const PopupLikeComment = ({ id, children }) => {
  const getReactions = () =>
    commentReactionService.getCommentReactions({
      commentId: id,
      isLike: true
    });
  return <Popup getReactions={getReactions}>{children}</Popup>;
};

PopupLikeComment.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};

export { PopupLikeComment };
