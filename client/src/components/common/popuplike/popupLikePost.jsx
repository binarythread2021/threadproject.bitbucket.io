import React from 'react';
import PropTypes from 'prop-types';
import { postReaction as postReactionService } from 'src/services/services';
import { Popup } from './popup';

const PopupLikePost = ({ id, children }) => {
  const getReactions = () =>
    postReactionService.getPostReactions({
      postId: id,
      isLike: true
    });
  return <Popup getReactions={getReactions}>{children}</Popup>;
};

PopupLikePost.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};

export { PopupLikePost };
