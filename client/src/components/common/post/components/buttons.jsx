import React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor } from 'src/common/enums/enums';
import { Button } from 'src/components/common/common';

const ButtonsChange = ({ onDelete, onUpdate }) =>
  onDelete || onUpdate ? (
    <div>
      {onDelete && (
        <Button color={ButtonColor.BLUE} onClick={onDelete}>
          Delete
        </Button>
      )}
      {onUpdate && (
        <Button color={ButtonColor.BLUE} onClick={onUpdate}>
          Update
        </Button>
      )}
    </div>
  ) : null;

ButtonsChange.propTypes = {
  onDelete: PropTypes.func,
  onUpdate: PropTypes.func
};

ButtonsChange.defaultProps = {
  onDelete: undefined,
  onUpdate: undefined
};

export { ButtonsChange };
