import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import styles from '../styles.module.scss';

const SocialButton = ({ count, onClick, iconName }) => (
  <Label
    basic
    size="small"
    as="a"
    className={styles.toolbarBtn}
    onClick={onClick}
  >
    <Icon name={IconName[iconName]} />
    {!!count && count}
  </Label>
);

SocialButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  iconName: PropTypes.string.isRequired,
  count: PropTypes.string
};

SocialButton.defaultProps = {
  count: undefined
};

export { SocialButton };
