import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { postType } from 'src/common/prop-types/prop-types';
import { Card, Image } from 'src/components/common/common';
import { ButtonsChange, SocialButton } from './components';
import { PopupLikePost } from '../popuplike';
import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onExpandedPostToggle,
  onPostDelete,
  sharePost,
  userId,
  updatePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);
  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostLike(id, false);
  const handlePostDelete = () => onPostDelete && onPostDelete(id);
  const handlePostUpdate = () => updatePost && updatePost(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className={styles.wrapper}>
          <div className={styles.leftColumn}>
            <PopupLikePost id={id}>
              <SocialButton
                count={likeCount}
                onClick={handlePostLike}
                iconName="THUMBS_UP"
              />
            </PopupLikePost>
            <SocialButton
              count={dislikeCount}
              onClick={handlePostDislike}
              iconName="THUMBS_DOWN"
            />
            <SocialButton
              count={commentCount}
              onClick={handleExpandedPostToggle}
              iconName="COMMENT"
            />
            <SocialButton
              onClick={() => sharePost(id)}
              iconName="SHARE_ALTERNATE"
            />
          </div>
          {post.userId === userId ? (
            <ButtonsChange
              onDelete={handlePostDelete}
              onUpdate={handlePostUpdate}
            />
          ) : null}
        </div>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func,
  updatePost: PropTypes.func,
  userId: PropTypes.string
};

Post.defaultProps = {
  userId: '',
  onPostDelete: undefined,
  updatePost: undefined
};

export default Post;
