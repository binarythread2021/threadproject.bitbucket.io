import { configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';

import { profileReducer, threadReducer } from './root-reducer';

const store = configureStore({
  reducer: {
    profile: profileReducer,
    posts: threadReducer
  },
  middleware: getDefaultMiddleware => getDefaultMiddleware().concat(logger)
});

export default store;
