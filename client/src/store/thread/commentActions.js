import { NotificationManager } from 'react-notifications';
import { comment as commentService } from 'src/services/services';
import { setPosts, setExpandedPost } from './actions';
import { updatePosts } from './helpers/updatePosts';

const addComment = request => async (dispatch, getRootState) => {
  try {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);
    const {
      posts: { posts, expandedPost }
    } = getRootState();
    const updatedPosts = updatePosts.updateComment(posts, comment, 'add');

    dispatch(setPosts(updatedPosts));
    if (expandedPost && expandedPost.id === comment.postId) {
      dispatch(
        setExpandedPost(updatePosts.updateComment(expandedPost, comment, 'add'))
      );
    }
  } catch (e) {
    NotificationManager.error(e.message);
  }
};

const deleteComment = (commentId, postId) => async (dispatch, getState) => {
  try {
    await commentService.deleteComment(commentId);
    const {
      posts: { posts, expandedPost }
    } = getState();
    const updatedPosts = updatePosts.updateComment(posts, { postId }, 'delete');
    dispatch(setPosts(updatedPosts));
    if (expandedPost && expandedPost.id === postId) {
      dispatch(
        setExpandedPost(
          updatePosts.updateComment(expandedPost, { id: commentId }, 'delete')
        )
      );
    }
  } catch (e) {
    NotificationManager.error(e.message);
  }
};

const updatedComment = (commentId, body) => async (dispatch, getState) => {
  try {
    await commentService.updateComment(commentId, body);
    const newComment = await commentService.getComment(commentId);
    const {
      posts: { expandedPost }
    } = getState();
    if (expandedPost && expandedPost.id === newComment.postId) {
      const newPost = {
        ...expandedPost,
        comments: expandedPost.comments.map(comment =>
          comment.id !== commentId ? comment : newComment
        )
      };
      dispatch(setExpandedPost(newPost));
    }
  } catch (e) {
    NotificationManager.error(e.message);
  }
};

function likeComment(commentId, postId, isLike = true) {
  return async (dispatch, getState) => {
    try {
      await commentService.likeComment(commentId, isLike);
      const newCommit = await commentService.getComment(commentId);
      const {
        posts: { expandedPost }
      } = getState();
      if (expandedPost && expandedPost.id === postId) {
        const newPost = {
          ...expandedPost,
          comments: expandedPost.comments.map(comment =>
            comment.id !== commentId ? comment : newCommit
          )
        };
        dispatch(setExpandedPost(newPost));
      }
    } catch (e) {
      NotificationManager.error(e.message);
    }
  };
}

export { addComment, likeComment, deleteComment, updatedComment };
