/* eslint-disable arrow-body-style */
import { createAction } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import {
  post as postService,
  image as imageService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST: 'thread/update-post',
  DELETE_POST: 'thread/delete-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const updatePostAction = createAction(ActionType.UPDATE_POST, post => ({
  payload: post
}));

const deletePostAction = createAction(ActionType.DELETE_POST, payload => ({
  payload
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = ({ body, image }) => {
  return async dispatch => {
    try {
      const post = { body };
      if (image) {
        const uploadedImage = await imageService.uploadImage(image);
        Object.assign(post, { imageId: uploadedImage.id });
      }
      const { id } = await postService.addPost(post);
      const newPost = await postService.getPost(id);
      dispatch(addPost(newPost));
    } catch (e) {
      NotificationManager.error(e?.message ?? 'wrong');
    }
  };
};

const updatePost = ({ image, body, post }) => {
  return async dispatch => {
    try {
      const payload = { body };
      if ((image && post?.imageId) || (!image && post?.imageId)) {
        const data = await imageService.deleteImage(post.imageId);
        if (data?.delete !== 'success') {
          throw new Error('wrong data!');
        }
      }
      if (image) {
        const uploadedImage = await imageService.uploadImage(image);
        Object.assign(payload, { imageId: uploadedImage.id });
      }
      const uploadedPost = await postService.updatePost(post.id, payload);
      const newPost = await postService.getPost(uploadedPost.id);
      dispatch(updatePostAction(newPost));
    } catch (e) {
      NotificationManager.error(e?.message ?? 'Unknow error');
    }
  };
};

const deletePost = id => async dispatch => {
  try {
    await postService.deletePost(id);
    dispatch(deletePostAction(id));
  } catch (e) {
    NotificationManager.error(e.message);
  }
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = (postId, isLike = true) => {
  return async (dispatch, getState) => {
    await postService.likePost(postId, isLike);
    const newPost = await postService.getPost(postId);
    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : newPost));

    dispatch(setPosts(updated));

    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPost(newPost));
    }
  };
};

export {
  setPosts,
  updatePostAction,
  addMorePosts,
  addPost,
  deletePost,
  deletePostAction,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  toggleExpandedPost,
  likePost
};
