class UpdatePosts {
  constructor() {
    this.links = {
      add: this._addComment,
      delete: this._deleteComment
    };
  }

  updateComment(posts, comment, type) {
    const method = this.links[type];
    if (!Array.isArray(posts)) {
      return method(posts, comment);
    }
    return posts.map(post => {
      if (post.id !== comment.postId) {
        return post;
      }
      return method(post, comment);
    });
  }

  _addComment(post, comment, value = 1) {
    return {
      ...post,
      commentCount: `${Number(post.commentCount) + value}`,
      comments: [...(post.comments || []), comment]
    };
  }

  _deleteComment(post, { id }, value = -1) {
    return {
      ...post,
      commentCount: `${Number(post.commentCount) + value}`,
      comments: post.comments.filter(comment => comment.id !== id)
    };
  }
}

const updatePosts = new UpdatePosts();

export { updatePosts };
