import { createReducer } from '@reduxjs/toolkit';
import {
  setUser,
  setToken,
  setLoadStartPage,
  setLoading,
  setUsers
} from './actions';

const initialState = {
  user: null,
  loadingStartPage: true,
  token: null,
  loading: false,
  users: []
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUser, (state, action) => {
    state.user = action.payload;
  });
  builder.addCase(setToken, (state, action) => {
    state.token = action.payload;
  });
  builder.addCase(setLoadStartPage, (state, action) => {
    state.loadingStartPage = action.payload;
  });
  builder.addCase(setLoading, (state, action) => {
    state.loading = action.payload;
  });
  builder.addCase(setUsers, (state, action) => {
    state.users = action.payload;
  });
});

export { reducer };
