import { createAction } from '@reduxjs/toolkit';
import { NotificationManager } from 'react-notifications';
import { StorageKey } from 'src/common/enums/enums';
import {
  storage as storageService,
  auth as authService,
  user as userService,
  image as imageService
} from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user',
  SET_TOKEN: 'profile/set-token',
  SET_LOAD: 'profile/set-load',
  SET_USERS: 'profile/set-users',
  SET_LOAD_PROFILE: 'profile/set-load-profile'
};

const setUser = createAction(ActionType.SET_USER, payload => ({ payload }));

const setUsers = createAction(ActionType.SET_USERS, payload => ({ payload }));

const setLoadStartPage = createAction(ActionType.SET_LOAD, payload => ({
  payload
}));

const setLoading = createAction(ActionType.SET_LOAD_PROFILE, payload => ({
  payload
}));

const setToken = createAction(ActionType.SET_TOKEN, payload => ({ payload }));

const login = request => async dispatch => {
  dispatch(setLoading(true));
  try {
    const { user, token } = await authService.login(request);
    storageService.setItem(StorageKey.TOKEN, token);
    dispatch(setUser(user));
    dispatch(setToken(token));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'error');
  }
  dispatch(setLoading(false));
};

const register = request => async dispatch => {
  dispatch(setLoading(true));
  try {
    const { user, token } = await authService.registration(request);
    storageService.setItem(StorageKey.TOKEN, token);
    dispatch(setUser(user));
    dispatch(setToken(token));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'error');
  }
  dispatch(setLoading(false));
};

const logout = () => dispatch => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
  dispatch(setToken(null));
};

const loadCurrentUser = token => async dispatch => {
  if (!token) {
    dispatch(setLoadStartPage(false));
    return;
  }
  dispatch(setToken(token));
  try {
    dispatch(setLoadStartPage(true));
    const user = await authService.getCurrentUser();
    dispatch(setUser(user));
  } catch (e) {
    if (e.name === 'UnauthorizedError') {
      storageService.removeItem(StorageKey.TOKEN);
    }
  } finally {
    dispatch(setLoadStartPage(false));
  }
};

const uploadFields = (fieldName, fieldValue) => async dispatch => {
  dispatch(setLoading(true));
  try {
    const { upload } = await userService.uploadFields({
      fieldName,
      fieldValue
    });
    if (upload !== 'success') {
      throw new Error('somthing wrong!');
    }
    const user = await authService.getCurrentUser();
    dispatch(setUser(user));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'Unknow error');
  }
  dispatch(setLoading(false));
};

const uploadImage = (image, user) => async dispatch => {
  dispatch(setLoading(true));
  try {
    if (user?.image?.id) {
      const deleteImage = await imageService.deleteImage(user.image.id);
      if (deleteImage?.delete !== 'success') {
        throw new Error('error delete');
      }
    }
    const uploadedImage = await imageService.uploadImage(image);
    if (!uploadedImage?.id) {
      throw new Error('somthing wrong!');
    }
    const { upload } = await userService.uploadImage({
      imageId: uploadedImage.id
    });
    if (upload !== 'success') {
      throw new Error('wrong save new avatar');
    }
    const updatedUser = await authService.getCurrentUser();
    dispatch(setUser(updatedUser));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'Unknow error');
  }
  dispatch(setLoading(false));
};

const fogotPassword = request => async dispatch => {
  dispatch(setLoading(true));
  try {
    const data = await authService.fogotPassword(request);
    if (!data?.message) {
      throw new Error('somthing wrong');
    } else {
      NotificationManager.info(data.message);
    }
  } catch (e) {
    NotificationManager.error(e?.message ?? 'Unknow error');
  }
  dispatch(setLoading(false));
};

const resetPassword = request => async dispatch => {
  dispatch(setLoading(true));
  try {
    const data = await authService.resetPassword(request);
    if (!data?.token || !data?.user) {
      throw new Error(data?.message ?? 'somthing wrong');
    }
    storageService.setItem(StorageKey.TOKEN, data.token);
    dispatch(setUser(data.user));
    dispatch(setToken(data.token));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'Unknow error');
  }
  dispatch(setLoading(false));
};

const getAllUsers = () => async dispatch => {
  dispatch(setLoading(true));
  try {
    const users = await userService.getAllUsers();
    dispatch(setUsers(users));
  } catch (e) {
    NotificationManager.error(e?.message ?? 'Unknow error');
  }
  dispatch(setLoading(false));
};

export {
  setUser,
  setToken,
  setLoadStartPage,
  setLoading,
  setUsers,
  login,
  register,
  logout,
  loadCurrentUser,
  uploadFields,
  uploadImage,
  fogotPassword,
  resetPassword,
  getAllUsers
};
