const checkResetExpires = date => date > Date.now();

export { checkResetExpires };
