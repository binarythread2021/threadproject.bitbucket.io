import { ENV } from '../../../common/enums/enums';

const resetExpires = () => Date.now() + ENV.RESET_PASSWORD.EXPIRES;

export { resetExpires };
