import crypto from 'crypto';
import { encrypt } from '../../crypt/crypt';

const createTokenResetPassword = async () => {
  const token = crypto.randomBytes(32).toString('hex');
  const resetToken = await encrypt(token);
  return { token, resetToken };
};

export { createTokenResetPassword };
