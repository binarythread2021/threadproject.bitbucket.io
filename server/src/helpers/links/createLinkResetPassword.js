import { ENV, AuthApiPath } from '../../common/enums/enums';

export const createLinkResetPassword = (resetToken, userId) =>
  `${ENV.APP.URL}${AuthApiPath.RESET_PASSWORD}?token=${resetToken}&id=${userId}`;
