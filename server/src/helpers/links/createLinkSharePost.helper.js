import { ENV } from '../../common/enums/enums';

const createLinkSharePost = postId => `${ENV.APP.URL}/share/${postId}`;

export { createLinkSharePost };
