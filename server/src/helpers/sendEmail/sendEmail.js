import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import fs from 'fs';
import path from 'path';
import { ENV } from '../../common/enums/enums';

class SendEmail {
  getTransporter() {
    return nodemailer.createTransport({
      host: ENV.EMAIL.HOST,
      port: ENV.EMAIL.PORT,
      auth: {
        user: ENV.EMAIL.USERNAME,
        pass: ENV.EMAIL.PASSWORD
      }
    });
  }

  getHtml(payload, template) {
    const source = fs.readFileSync(path.join(__dirname, template), 'utf8');
    const compiledTemplate = handlebars.compile(source);
    return compiledTemplate(payload);
  }

  async fogotPassword(email, payload) {
    try {
      const options = () => ({
        from: ENV.EMAIL.FROM,
        to: email,
        subject: 'Reset Password',
        html: this.getHtml(payload, ENV.TEMPLATE.FOGOT_PASSWORD)
      });
      const transporter = this.getTransporter();
      const info = await transporter.sendMail(options());
      if (info?.error) {
        throw new Error(info.error?.message ?? 'Error send mail');
      }
      return { success: true };
    } catch (error) {
      throw new Error(error?.message ?? 'Send Error');
    }
  }

  async shareLikePost(email, payload) {
    try {
      const options = () => ({
        from: ENV.EMAIL.FROM,
        to: email,
        subject: 'Your post is liked',
        html: this.getHtml(payload, ENV.TEMPLATE.LIKED_POST)
      });
      const transporter = this.getTransporter();
      const info = await transporter.sendMail(options());
      if (info?.error) {
        throw new Error(info.error?.message ?? 'Error send mail');
      }
      return { success: true };
    } catch (error) {
      throw new Error(error?.message ?? 'Send Error');
    }
  }

  async sharePost(email, payload) {
    try {
      const options = () => ({
        from: ENV.EMAIL.FROM,
        to: email,
        subject: 'Shared post',
        html: this.getHtml(payload, ENV.TEMPLATE.SHARED_POST)
      });
      const transporter = this.getTransporter();
      const info = await transporter.sendMail(options());
      if (info?.error) {
        throw new Error(info.error?.message ?? 'Error send mail');
      }
      return { success: true };
    } catch (error) {
      throw new Error(error?.message ?? 'Send Error');
    }
  }
}

const sendEmail = new SendEmail();

export { sendEmail };
