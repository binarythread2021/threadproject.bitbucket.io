import { CustomError } from './error';

export class ValidationError extends CustomError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}
