import { CustomError } from './error';

export class InternalError extends CustomError {}
