import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel, postModel, userModel, imageModel }) {
    super(postReactionModel);
    this._postModel = postModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
  }

  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: ['postReaction.id', 'post.id'],
      where: { userId, postId },
      include: [
        {
          model: this._postModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }

  async getPostReactions(filter) {
    const { postId, isLike } = filter;
    const where = {};
    if (postId && isLike !== undefined) {
      Object.assign(where, { postId, isLike });
    }
    return this.model.findAll({
      where,
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: [
            {
              model: this._imageModel,
              attributes: ['id', 'link'],
              duplicating: false
            }
          ]
        }
      ]
    });
  }
}

export { PostReaction };
