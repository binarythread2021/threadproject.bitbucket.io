import {
  CommentModel,
  UserModel,
  ImageModel,
  PostModel,
  PostReactionModel,
  CommentReactionModel
} from '../models';
import { Comment } from './comment/comment.repository';
import { Image } from './image/image.repository';
import { PostReaction } from './post-reaction/post-reaction.repository';
import { CommentReaction } from './comment-reaction/comment-reaction.repository';
import { Post } from './post/post.repository';
import { User } from './user/user.repository';

const comment = new Comment({
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel,
  commentReactionModel: CommentReactionModel
});

const image = new Image({
  imageModel: ImageModel
});

const postReaction = new PostReaction({
  postReactionModel: PostReactionModel,
  postModel: PostModel,
  userModel: UserModel,
  imageModel: ImageModel
});

const commentReaction = new CommentReaction({
  commentReactionModel: CommentReactionModel,
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel
});

const post = new Post({
  postModel: PostModel,
  commentModel: CommentModel,
  commentReactionModel: CommentReactionModel,
  userModel: UserModel,
  imageModel: ImageModel,
  postReactionModel: PostReactionModel
});

const user = new User({
  userModel: UserModel,
  imageModel: ImageModel
});

export { comment, image, postReaction, post, user, commentReaction };
