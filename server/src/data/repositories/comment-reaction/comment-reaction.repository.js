import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel, commentModel, userModel, imageModel }) {
    super(commentReactionModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id'],
      where: { userId, commentId },
      include: [
        {
          model: this._commentModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }

  getCommentReactions(filter) {
    const { commentId, isLike } = filter;
    const where = {};
    if (commentId && isLike !== undefined) {
      Object.assign(where, { commentId, isLike });
    }
    return this.model.findAll({
      where,
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: [
            {
              model: this._imageModel,
              attributes: ['id', 'link'],
              duplicating: false
            }
          ]
        }
      ]
    });
  }
}

export { CommentReaction };
