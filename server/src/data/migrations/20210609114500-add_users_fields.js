export default {
  up: (queryInterface, Sequelize) =>
    queryInterface.sequelize.transaction(transaction =>
      Promise.all([
        queryInterface.addColumn(
          'users',
          'resetToken',
          {
            type: Sequelize.STRING,
            allowNull: true
          },
          { transaction }
        ),
        queryInterface.addColumn(
          'users',
          'resetExpires',
          {
            type: Sequelize.DATE,
            allowNull: true
          },
          { transaction }
        )
      ])
    ),

  down: queryInterface =>
    queryInterface.sequelize.transaction(transaction =>
      Promise.all([
        queryInterface.removeColumn('users', 'ressetToken', { transaction }),
        queryInterface.removeColumn('users', 'ressetExpires', { transaction })
      ])
    )
};
