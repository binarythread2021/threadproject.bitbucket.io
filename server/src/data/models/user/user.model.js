import { DataTypes } from 'sequelize';

const init = orm => {
  const User = orm.define(
    'user',
    {
      email: {
        allowNull: false,
        type: DataTypes.STRING
      },
      username: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true
      },
      status: {
        allowNull: true,
        type: DataTypes.STRING
      },
      resetToken: {
        allowNull: true,
        type: DataTypes.STRING
      },
      resetExpires: {
        allowNull: true,
        type: DataTypes.DATE
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return User;
};

export { init };
