import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService, commentReaction: commentReactionService } =
    services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) =>
      commentService
        .getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next)
    )
    .post(CommentsApiPath.ROOT, (req, res, next) =>
      commentService
        .create(req.user.id, req.body)
        .then(comment => res.send(comment))
        .catch(next)
    )
    .post(CommentsApiPath.REACT, (req, res, next) =>
      commentReactionService
        .getComments(req.body)
        .then(reactions => res.send(reactions))
        .catch(next)
    )
    .put(CommentsApiPath.REACT, (req, res, next) =>
      commentService
        .setReaction(req.user.id, req.body)
        .then(reaction => {
          if (reaction.comment && reaction.comment.userId !== req.user.id) {
            const message = req.body.isLike
              ? 'Your comment was liked'
              : 'Your comment was disliked';
            req.io.to(reaction.comment.userId).emit('like', message);
          }
          return res.send(reaction);
        })
        .catch(next)
    )
    .put(CommentsApiPath.$ID, (req, res, next) =>
      commentService
        .update(req.user.id, req.params.id, req.body)
        .then(data => res.send(data))
        .catch(next)
    )
    .delete(CommentsApiPath.$ID, (req, res, next) =>
      commentService
        .delete(req.user.id, req.params.id)
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initComment };
