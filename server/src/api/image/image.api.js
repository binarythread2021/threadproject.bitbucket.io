import { ImagesApiPath } from '../../common/enums/enums';
import { image as imageMiddleware } from '../../middlewares/middlewares';

const initImage = (Router, services) => {
  const { image: imageService } = services;
  const router = Router();

  router.post(ImagesApiPath.ROOT, imageMiddleware, (req, res, next) =>
    imageService
      .upload(req.file)
      .then(image => res.send(image))
      .catch(next)
  );

  router.delete(ImagesApiPath.$ID, (req, res, next) =>
    imageService
      .delete(req.params.id)
      .then(data => res.send(data))
      .catch(next)
  );

  return router;
};

export { initImage };
