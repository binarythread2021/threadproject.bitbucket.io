import { ApiPath } from '../common/enums/enums';
import {
  auth,
  user,
  comment,
  post,
  image,
  postReaction,
  commentReaction
} from '../services/services';
import { initAuth } from './auth/auth.api';
import { initUser } from './user/user.api';
import { initPost } from './post/post.api';
import { initComment } from './comment/comment.api';
import { initImage } from './image/image.api';

// register all routes
const initApi = Router => {
  const apiRouter = Router();

  apiRouter.use(
    ApiPath.AUTH,
    initAuth(Router, {
      auth,
      user
    })
  );
  apiRouter.use(
    ApiPath.USER,
    initUser(Router, {
      user
    })
  );
  apiRouter.use(
    ApiPath.POSTS,
    initPost(Router, {
      post,
      postReaction
    })
  );
  apiRouter.use(
    ApiPath.COMMENTS,
    initComment(Router, {
      comment,
      commentReaction
    })
  );
  apiRouter.use(
    ApiPath.IMAGES,
    initImage(Router, {
      image
    })
  );

  return apiRouter;
};

export { initApi };
