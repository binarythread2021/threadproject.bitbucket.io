import { PostsApiPath } from '../../common/enums/enums';

const initPost = (Router, services) => {
  const { post: postService, postReaction: postReactionService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) =>
      postService
        .getPosts(req.query)
        .then(posts => res.send(posts))
        .catch(next)
    )
    .get(PostsApiPath.$ID, (req, res, next) =>
      postService
        .getPostById(req.params.id)
        .then(post => res.send(post))
        .catch(next)
    )
    .post(PostsApiPath.ROOT, (req, res, next) =>
      postService
        .create(req.user.id, req.body)
        .then(post => {
          req.io.emit('new_post', post); // notify all users that a new post was created
          return res.send(post);
        })
        .catch(next)
    )
    .post(PostsApiPath.REACT, (req, res, next) =>
      postReactionService
        .getPosts(req.body)
        .then(reactions => res.send(reactions))
        .catch(next)
    )
    .post(PostsApiPath.SHARE, (req, res, next) =>
      postService
        .sendSharePost(req.user, req.body)
        .then(data => res.send(data))
        .catch(next)
    )
    .put(PostsApiPath.REACT, (req, res, next) =>
      postService
        .setReaction(req.user.id, req.body)
        .then(reaction => {
          if (reaction.post && reaction.post.userId !== req.user.id) {
            // notify a user if someone (not himself) liked his post
            const message = req.body.isLike
              ? 'Your comment was liked'
              : 'Your comment was disliked';
            req.io.to(reaction.post.userId).emit('like', message);
          }
          return res.send(reaction);
        })
        .catch(next)
    )
    .put(PostsApiPath.$ID, (req, res, next) =>
      postService
        .update(req.user.id, req.params.id, req.body)
        .then(post => res.send(post))
        .catch(next)
    )
    .delete(PostsApiPath.$ID, (req, res, next) =>
      postService
        .delete(req.user.id, req.params.id)
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initPost };
