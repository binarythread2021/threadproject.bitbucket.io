import { UserApiPath } from '../../common/enums/enums';

const initUser = (Router, services) => {
  const { user: userService } = services;
  const router = Router();

  router
    .put(UserApiPath.PROFILE, (req, res, next) =>
      userService
        .uploadFields(req.user.id, req.body.fieldName, req.body.fieldValue)
        .then(data => res.send(data))
        .catch(next)
    )
    .put(UserApiPath.IMAGE, (req, res, next) =>
      userService
        .uploadImage(req.user.id, req.body.imageId)
        .then(data => res.send(data))
        .catch(next)
    )
    .get(UserApiPath.ROOT, (_, res, next) =>
      userService
        .getAllUsers()
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initUser };
