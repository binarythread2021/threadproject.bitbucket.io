import {
  ValidationError,
  InternalError,
  createLinkSharePost,
  sendEmail
} from '../../helpers/helpers';

class Post {
  constructor({ postRepository, postReactionRepository, userRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
    this._userRepository = userRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async update(userId, postId, post) {
    const {
      user: { id }
    } = await this._postRepository.getPostById(postId);
    if (id !== userId) {
      throw new ValidationError('wrong user');
    }
    return this._postRepository.updateById(postId, {
      ...post,
      userId
    });
  }

  async delete(userId, postId) {
    const {
      user: { id }
    } = await this._postRepository.getPostById(postId);
    if (id !== userId) {
      throw new ValidationError('wrong post');
    }
    const data = await this._postRepository.deleteById(postId);
    if (!Number.isInteger(data)) {
      throw new InternalError('Internal Error');
    }
    return { delete: 'success' };
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react =>
      react.isLike === isLike
        ? this._postReactionRepository.deleteById(react.id)
        : this._postReactionRepository.updateById(react.id, { isLike });

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );
    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({
          userId,
          postId,
          isLike
        });
    // the result is an integer when an entity is deleted
    const postReaction = Number.isInteger(result)
      ? {}
      : await this._postReactionRepository.getPostReaction(userId, postId);
    if (!postReaction?.post) {
      return postReaction;
    }
    const { email, username } = await this._userRepository.getUserById(
      postReaction.userId
    );
    const link = createLinkSharePost(postId);
    await sendEmail.shareLikePost(email, {
      name: username,
      link
    });
    return postReaction;
  }

  async sendSharePost(user, { userId, postId }) {
    const { email } = await this._userRepository.getUserById(userId);
    const link = createLinkSharePost(postId);
    const response = await sendEmail.sharePost(email, {
      name: user.username,
      link
    });
    if (response?.success) {
      return {
        ...response,
        message: 'email was sent'
      };
    }
    return {
      message: 'email not sent'
    };
  }
}

export { Post };
