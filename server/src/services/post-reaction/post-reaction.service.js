class PostReaction {
  constructor({ postReactionRepository }) {
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postReactionRepository.getPostReactions(filter);
  }
}

export { PostReaction };
