import {
  encrypt,
  cryptCompare,
  createToken,
  ValidationError,
  sendEmail,
  createLinkResetPassword,
  createTokenResetPassword,
  resetExpires,
  checkResetExpires
} from '../../helpers/helpers';

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const newUser = await this._userRepository.addUser({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async fogotPassword({ email }) {
    const user = await this._userRepository.getByEmail(email);
    if (!user) {
      throw new ValidationError('Email is not exist!');
    }
    const { token, resetToken } = await createTokenResetPassword();
    await this._userRepository.updateById(user.id, {
      resetToken,
      resetExpires: resetExpires()
    });
    const info = await sendEmail.fogotPassword(email, {
      name: user.username,
      link: createLinkResetPassword(token, user.id)
    });
    return {
      ...info,
      message: 'Check your email'
    };
  }

  async resetPassword({ token, userId, password }) {
    const user = await this._userRepository.getUserForReset(userId);
    if (!user) {
      throw new ValidationError('no token');
    }
    if (
      !checkResetExpires(user.resetExpires) ||
      !cryptCompare(token, user.resetToken)
    ) {
      throw new ValidationError('Wrong token');
    }
    const userWithResetPassword = await this._userRepository.updateById(
      userId,
      {
        password: await encrypt(password),
        resetPassword: null,
        resetExpires: null
      }
    );
    return this.login(userWithResetPassword);
  }
}

export { Auth };
