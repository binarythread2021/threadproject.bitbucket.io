import {
  user as userRepository,
  comment as commentRepository,
  image as imageRepository,
  post as postRepository,
  postReaction as postReactionRepository,
  commentReaction as commentReactionRepository
} from '../data/repositories/repositories';
import { Auth } from './auth/auth.service';
import { Comment } from './comment/comment.service';
import { Http } from './http/http.service';
import { Image } from './image/image.service';
import { Post } from './post/post.service';
import { User } from './user/user.service';
import { PostReaction } from './post-reaction/post-reaction.service';
import { CommentReaction } from './comment-reaction/comment-reaction.service';

const http = new Http();

const auth = new Auth({
  userRepository
});

const comment = new Comment({
  commentRepository,
  commentReactionRepository
});

const commentReaction = new CommentReaction({
  commentReactionRepository
});

const image = new Image({
  http,
  imageRepository,
  postRepository
});

const post = new Post({
  postRepository,
  postReactionRepository,
  userRepository
});

const postReaction = new PostReaction({ postReactionRepository });

const user = new User({
  userRepository
});

export { auth, comment, image, post, user, postReaction, commentReaction };
