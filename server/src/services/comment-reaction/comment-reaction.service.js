class CommentReaction {
  constructor({ commentReactionRepository }) {
    this._commentReactionRepository = commentReactionRepository;
  }

  getComments(filter) {
    return this._commentReactionRepository.getCommentReactions(filter);
  }
}

export { CommentReaction };
