import { ValidationError, InternalError } from '../../helpers/helpers';
import { USER_UPLOAD_FIELDS } from '../../common/constants/constants';

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);
    return user;
  }

  async getAllUsers() {
    const users = await this._userRepository.getAllUsers();
    return users;
  }

  async uploadFields(id, fieldName, fieldValue) {
    if (fieldName === 'name') {
      await this.checkUsername(fieldValue);
    }
    if (!USER_UPLOAD_FIELDS.some(filed => filed === fieldName)) {
      throw new ValidationError('Wrong field');
    }
    const user = await this._userRepository.updateById(id, {
      [fieldName]: fieldValue
    });
    if (!user?.id) {
      throw new InternalError('Internal Error');
    }
    return { upload: 'success' };
  }

  async uploadImage(id, imageId) {
    const user = await this._userRepository.updateById(id, { imageId });
    if (!user?.id) {
      throw new InternalError('Internal Error');
    }
    return { upload: 'success' };
  }

  async checkUsername(name) {
    const isName = await this._userRepository.getByUsername(name);
    if (isName) {
      throw new ValidationError('Such name already exists');
    }
  }
}

export { User };
