import { ENV, HttpMethod } from '../../common/enums/enums';
import { InternalError } from '../../helpers/helpers';

class Image {
  constructor({ http, imageRepository, postRepository }) {
    this._imageRepository = imageRepository;
    this._postRepository = postRepository;
    this._http = http;
  }

  async upload(file) {
    const { data } = await this._http.load(ENV.IMGUR.UPLOAD_API_URL, {
      method: HttpMethod.POST,
      data: {
        image: file.buffer.toString('base64')
      },
      headers: { Authorization: `Client-ID ${ENV.IMGUR.ID}` }
    });

    return this._imageRepository.create({
      link: data.link,
      deleteHash: data.deletehash
    });
  }

  async delete(imageId) {
    const image = await this._imageRepository.getById(imageId);
    const dataFromImgur = await this._http.load(
      `${ENV.IMGUR.UPLOAD_API_URL}/${image.deleteHash}`,
      {
        method: HttpMethod.DELETE,
        headers: { Authorization: `Client-ID ${ENV.IMGUR.ID}` }
      }
    );
    const data = await this._imageRepository.deleteById(imageId);
    if (!Number.isInteger(data)) {
      throw new InternalError('Internal Error');
    }
    return { delete: 'success' };
  }
}

export { Image };
