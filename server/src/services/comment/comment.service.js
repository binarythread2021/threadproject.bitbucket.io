class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  async update(userId, commentId, comment) {
    const {
      user: { id }
    } = await this._commentRepository.getCommentById(commentId);
    if (id !== userId) {
      throw new SyntaxError('Error data');
    }
    return this._commentRepository.updateById(commentId, {
      ...comment,
      userId
    });
  }

  async delete(userId, commentId) {
    const {
      user: { id }
    } = await this._commentRepository.getCommentById(commentId);
    if (id !== userId) {
      throw new SyntaxError('Error data');
    }
    const data = await this._commentRepository.deleteById(commentId);
    if (!Number.isInteger(data)) {
      throw new SyntaxError('Internal Error');
    }
    return { delete: 'success' };
  }

  async setReaction(userId, { commentId, isLike = true }) {
    const updateOrDelete = react =>
      react.isLike === isLike
        ? this._commentReactionRepository.deleteById(react.id)
        : this._commentReactionRepository.updateById(react.id, { isLike });

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );
    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({
          userId,
          commentId,
          isLike
        });

    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }
}

export { Comment };
