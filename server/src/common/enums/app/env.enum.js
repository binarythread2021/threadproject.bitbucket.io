import { config } from 'dotenv';

config();

const {
  APP_PORT,
  APP_URL,
  SOCKET_PORT,
  SECRET_KEY,
  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  DB_DIALECT,
  IMGUR_ID,
  IMGUR_SECRET,
  IMGUR_UPLOAD_API_URL,
  EMAIL_HOST,
  EMAIL_PORT,
  EMAIL_USERNAME,
  EMAIL_PASSWORD,
  EMAIL_FROM
} = process.env;

const ENV = {
  APP: {
    API_PATH: '/api',
    URL: APP_URL,
    PORT: APP_PORT,
    SOCKET_PORT
  },
  JWT: {
    SECRET: SECRET_KEY,
    EXPIRES_IN: '24h'
  },
  DB: {
    DATABASE: DB_NAME,
    USERNAME: DB_USERNAME,
    PASSWORD: DB_PASSWORD,
    HOST: DB_HOST,
    PORT: DB_PORT,
    DIALECT: DB_DIALECT,
    LOGGING: false
  },
  IMGUR: {
    ID: IMGUR_ID,
    SECRET: IMGUR_SECRET,
    UPLOAD_API_URL: IMGUR_UPLOAD_API_URL,
    FILE_SIZE: 10000000
  },
  EMAIL: {
    HOST: EMAIL_HOST,
    PORT: EMAIL_PORT,
    USERNAME: EMAIL_USERNAME,
    PASSWORD: EMAIL_PASSWORD,
    FROM: EMAIL_FROM
  },
  TEMPLATE: {
    FOGOT_PASSWORD: '../../templates/layouts/fogotPassword.handlebars',
    LIKED_POST: '../../templates/layouts/likedPost.handlebars',
    SHARED_POST: '../../templates/layouts/sharedPost.handlebars'
  },
  RESET_PASSWORD: {
    EXPIRES: 60 * 60 * 1000
  }
};

export { ENV };
