const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  FOGOT_PASSWORD: '/fogot-password',
  RESET_PASSWORD: '/reset-password'
};

export { AuthApiPath };
