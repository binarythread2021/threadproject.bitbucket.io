const ApiPath = {
  AUTH: '/auth',
  USER: '/user',
  POSTS: '/posts',
  COMMENTS: '/comments',
  IMAGES: '/images'
};

export { ApiPath };
