const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  SHARE: '/share'
};

export { PostsApiPath };
