const UserApiPath = {
  PROFILE: '/profile',
  IMAGE: '/image',
  ROOT: '/'
};

export { UserApiPath };
