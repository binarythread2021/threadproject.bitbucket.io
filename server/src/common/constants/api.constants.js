import { ApiPath, AuthApiPath } from '../enums/enums';

const WHITE_ROUTES = [
  `${ApiPath.AUTH}${AuthApiPath.LOGIN}`,
  `${ApiPath.AUTH}${AuthApiPath.REGISTER}`,
  `${ApiPath.AUTH}${AuthApiPath.FOGOT_PASSWORD}`,
  `${ApiPath.AUTH}${AuthApiPath.RESET_PASSWORD}`
];

export { WHITE_ROUTES };
